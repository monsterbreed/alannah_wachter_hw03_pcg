﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _01_Scripts
{
    public class MapGenerator : MonoBehaviour
    {
        [SerializeField]
        private GameBoard _gameBoard;
        [SerializeField]
        private SpriteRenderer _agent;
        [SerializeField]
        private bool _useSeed;
        [SerializeField]
        private int _seed;
        [SerializeField][Range(0, 100)]
        private float _groundPercentage;
        [SerializeField] 
        private float _maxSecondsToWait;
        [SerializeField][Range(1, 100)] 
        private float _generationSpeed;

        private System.Random _random;
        private SpriteRenderer _agentClone;
        private bool[,] _tileData;
        private bool _isGenerating;
        private List<Vector2> _groundTiles;
        private List<Vector2> _directions;

        private Vector2 _north;
        private Vector2 _south;
        private Vector2 _east;
        private Vector2 _west;

        private float _tilesToDraw()
        {
            var tilesCount = (_gameBoard.Tiles.Length / 100.0f) * _groundPercentage;
            return tilesCount;
        }

        private void Start()
        {
            _directions = new List<Vector2>();
            _groundTiles = new List<Vector2>();
            _tileData = new bool[_gameBoard.Width, _gameBoard.Height];
            
            InitRandom(); 
            SpawnAgent();
        }

        private void Update()
        {
            // TODO this should best be refactored into a "reset" method :)
            if (Input.GetMouseButtonDown(0))
            {
                for (int x = 0; x < _gameBoard.Width; x++)
                {
                    for (int y = 0; y < _gameBoard.Height; y++)
                    {
                        _tileData[x, y] = false;
                    }
                }
                InitRandom();
                _groundTiles.Clear();
                _gameBoard.PlotData(_tileData);
                SpawnAgent();
                _isGenerating = false;
            }

            if (_agentClone == null)
                return;

            // todo i would move this to a coroutine completely and start the coroutine just once. in the coroutine then we can check while (_groundTiles.Count < _tilesToDraw()) // do agenty stuff
            if(!_isGenerating && _groundTiles.Count < _tilesToDraw())
                StartCoroutine(GenerateMap());
        }

        // TODO as i stated above, this coroutine could handle the generation process completely on its own
        private IEnumerator GenerateMap()
        {
            _isGenerating = true;
            
            // TODO very interesting way of going about this! the only part I dont quite like about it, is that you iterate over the complete board each step
            // while this doesnt have a big impact anyways probably, its also easy to optimize that, by updating only the tileData of the tile that has just
            // changed. something like: transform currentTile = agentClone.transform; should be sufficient and the way you update the board etc is still pretty 
            // much the same. For a completely different approach of how to do this, look at my code! :D 
            if(!_groundTiles.Contains(_agentClone.transform.position))
                _groundTiles.Add(_agentClone.transform.position);

            for (int x = 0; x < _gameBoard.Width; x++)
            {
                for (int y = 0; y < _gameBoard.Height; y++)
                {
                    _tileData[x, y] = _groundTiles.Contains(_gameBoard.Tiles[x, y].transform.position);
                }
            }
            _gameBoard.PlotData(_tileData);
            
            UpdateDirection();
            _agentClone.transform.position = _directions[_random.Next(0, _directions.Count)];

            _directions.Clear();
            yield return new WaitForSeconds(_maxSecondsToWait/_generationSpeed);
            _isGenerating = false;
        }

        // TODO this was slightly confusing naming to me at first glance. I then figured what this does is rather update the available directions
        // this was not directly apparent for two reasons:
        // 1. _directions is a member of this class, so you just set it in this method
        // 2. this method doesnt return a value although this is what it essentially does
        private void UpdateDirection()
        {
            _directions.Clear();
            
            _north = (Vector2) _agentClone.transform.position + new Vector2(0, 1);
            _south = (Vector2) _agentClone.transform.position - new Vector2(0, 1);
            _east = (Vector2) _agentClone.transform.position + new Vector2(1, 0);
            _west = (Vector2) _agentClone.transform.position - new Vector2(1, 0);

            _directions.Add(_north);
            _directions.Add(_south);
            _directions.Add(_east);
            _directions.Add(_west);
            
            DetermineMapBorders();

            if (_tileData[(int) _west.x, (int) _west.y] && _tileData[(int) _east.x, (int) _east.y] &&
                _tileData[(int) _north.x, (int) _north.y] && _tileData[(int) _south.x, (int) _south.y])
                return;

            if (_tileData[(int)_west.x, (int)_west.y] && _directions.Contains(_west))
                _directions.Remove(_west);            
            if (_tileData[(int)_east.x, (int)_east.y] && _directions.Contains(_east))
                _directions.Remove(_east);            
            if (_tileData[(int)_north.x, (int)_north.y] && _directions.Contains(_north))
                _directions.Remove(_north);            
            if (_tileData[(int)_south.x, (int)_south.y] && _directions.Contains(_south))
                _directions.Remove(_south);

            if (_directions.Count == 0)
            {
                _directions.Add(_north);
                _directions.Add(_south);
                _directions.Add(_east);
                _directions.Add(_west);
            
                DetermineMapBorders();
            }
        }

        private void DetermineMapBorders()
        {
            if ((int)_west.x == 1)
                _directions.Remove(_west);            
            if ((int)_east.x == _gameBoard.Width - 1)
                _directions.Remove(_east);            
            if ((int)_north.y == _gameBoard.Height - 1)
                _directions.Remove(_north);            
            if ((int)_south.y == 1)
                _directions.Remove(_south);
        }

        private void SpawnAgent()
        {
            if(_agentClone != null)
                Destroy(_agentClone);
            
            var spawnTile = _gameBoard.Tiles[_random.Next(1, _gameBoard.Width - 1),_random.Next(1, _gameBoard.Height - 1)];
            _agentClone = Instantiate(_agent, spawnTile.transform);
        }

        private void InitRandom()
        {
            _random = _useSeed ? new System.Random(_seed) : new System.Random();
        }
    }
}
