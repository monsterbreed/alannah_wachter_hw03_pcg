﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _01_Scripts
{
	public class GameBoard : MonoBehaviour
	{
		[SerializeField] [Tooltip("Width of the board")]
		private int _width;

		[SerializeField] [Tooltip("Height of the board")]
		private int _height;

		[SerializeField] [Tooltip("Prefab used to generate the whole board.")]
		private SpriteRenderer _groundPrefab;

		// 2d array to store all tiles on the gameboard
		private SpriteRenderer[,] _tiles;

		public int Width => _width;
		public int Height => _height;

		public SpriteRenderer[,] Tiles => _tiles;

		private void Awake()
		{
			SpawnMap();
		}

		private void SpawnMap()
		{
			_tiles = new SpriteRenderer[_width, _height];
			for (int x = 0; x < _width; x++)
			{
				for (int y = 0; y < _height; y++)
				{
					Tiles[x, y] = Instantiate(_groundPrefab, gameObject.transform);
					Tiles[x, y].transform.position = new Vector3(x, y);
					// All tiles start as walls, which are represented by the color black
					Tiles[x, y].color = Color.black;
				}
			}
		}

		public void PlotData(bool[,] tileData)
		{
			for (int x = 0; x < _width; x++)
			{
				for (int y = 0; y < _height; y++)
				{
					Tiles[x, y].color = tileData[x, y] ? Color.white : Color.black;
				}
			}
		}
	}
}
